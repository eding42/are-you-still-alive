Are You Still Alive?
======

### *A useless tool, for useless people.*

Great at ~~parties~~ funerals!

Very useful for people who are at risk of or considering suicide. It might help you or harm you. Either one is possible.

The script asks the user if he or she is still alive, then records the result in a useful text file. If the user fails to respond within a certain period of time, the user is considered to have died.

## Setup

Please install the latest version of Python 3:

`sudo apt install python3`

The script doesn't work without Python installed. Wow, I didn't know that!

Make sure you install the package `python3` not `python`

`python` is Python 2.7, which uses a completely different set of syntax that will most likely break the script.

To run the script:

`python3 areyoustillalive.py`

or simply:

`chmod +x areyoustillalive.py`

and

`./areyoustillalive.py`

That should be good enough. 

The script outputs the status of your morality to a handy text file, named stillalive.txt. This is automatically created, if not cloned with the repo.

## Dependencies

I used the datetime module, along with threading and the os module. 

This was more of a learning project for me, as I've used this script to explore the possibilities of threading, and its uses in Python. 

Oh yeah, have I mentioned that you need Python installed? Okay, good. Thank you, and hopefully you stay alive long enough to see your dreams fall away. 

Chungus reigns supreme.

![alt text](https://stayhipp.com/wp-content/uploads/2018/12/chunkus.jpg "Chungus lol")